$(document).ready(function() {

	dragelm = $();
	dragelmtop = 0;
	draggables = $('li');

	draggables.attr('draggable', 'true');
	
	draggables.live('dragstart', function(ev) {
		//console.log('dragstart');
		dragelm = $(this);
		dragelmtop = dragelm.offset().top;
		$(this).addClass('drag');
		return true;
	});
	draggables.live('dragover', function(ev) {
		return false;
	});
	draggables.live('dragenter', function(ev) {
		//console.log('dragenter');
		$(this).addClass('drop');
		
		////console.log(ev);
		////console.log(ev.originalEvent.screenX + ' ' + ev.originalEvent.screenY);

		droptop = $(this).offset().top;
		droppos = 'none'
		
		if (droptop < dragelmtop ) {
			droppos = 'above'
		}
		else if (droptop > dragelmtop) {
			droppos = 'below'
		}
		else {
			droppos = 'ontop'
		};
		////console.log('droppos: ' + droppos);

		
		if (droppos == 'ontop') {
			draggables.removeClass('drop-prev').removeClass('drop-next').removeClass('drop-prev');
		}
		else if (droppos == 'above') {
			draggables.removeClass('drop-prev').removeClass('drop-next');
			$(this).addClass('drop-prev').nextUntil('.drag').addClass('drop-prev');
			////console.log('.drop-prev');
		}
		else if (droppos == 'below') {
			draggables.removeClass('drop-prev').removeClass('drop-next');
			$(this).addClass('drop-next').prevUntil('.drag').addClass('drop-next');
			////console.log('.drop-next');
		};

		return false;
	});
	draggables.live('dragleave', function(ev) {
		//console.log('dragleave');
		$(this).removeClass('drop');
		return false;
	});
	draggables.live('drop', function(ev) {
		//console.log('drop');
		ev.stopPropagation();
		dragelm.removeClass('drag');
		draggables.removeClass('drop').addClass('dropped');
		if ($(this).hasClass('drop-prev')) {
			$(this).before(dragelm);
		}
		else if ($(this).hasClass('drop-next')) {
			$(this).after(dragelm)
		};
		draggables.removeClass('drop-prev').removeClass('drop-next');
		setTimeout(function function_name (argument) {
			draggables.removeClass('dropped');
		}, 100);
		return false;
	});
	$('body').live('dragenter', function(event) {
		//console.log('body dragenter');
		draggables.removeClass('drop-prev').removeClass('drop-next').removeClass('drop');
	});
	draggables.live('dragend', function(ev) {
		//console.log('dragend');
		draggables.removeClass('drop').addClass('dropped');
		$('li.drop-prev:first').before(dragelm);
		$('li.drop-next:last').after(dragelm);
		draggables.removeClass('drop-prev').removeClass('drop-next').removeClass('drop-prev');
		setTimeout(function function_name (argument) {
			draggables.removeClass('dropped');
			dragelm.removeClass('drag');
		}, 100);
		return false;
	});

});